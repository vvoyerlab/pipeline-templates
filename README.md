# Catalogue de Templates GitLab

## Description
Ce projet propose un catalogue de templates GitLab pour faciliter la configuration et le déploiement de pipelines CI/CD. Il vise à fournir une base solide pour automatiser les tests, le déploiement et d'autres étapes clés des workflows de développement logiciel.

## Contribution
Les contributions sont les bienvenues ! Si vous souhaitez ajouter un nouveau template ou améliorer un template existant, veuillez suivre ces étapes :

1. Forkez le projet.
2. Créez votre branche de fonctionnalité (git checkout -b feature/NouveauTemplate).
3. Committez vos changements (git commit -am 'Ajout d'un nouveau template').
4. Poussez la branche (git push origin feature/NouveauTemplate).
5. Ouvrez une Merge Request.

## Structure d'un composant
Un composant doit être mis dans un sous dossier portant son nom dans le dossier templates et devra être structuré de la manière suivante:
```
templates/
└── component-1/
    ├── template.yml
    ├── CHANGELOG.md
    └── README.md
```

Le fichier README.md devra contenir la description du composant et une description de son utilisation.
Il devra contenir les métadonnées suivantes :
```markdown
---
title: Description
---
```

Le fichier Changelog devra contenir les différents changement qu'a subit le composant.
Il devra contenir les métadonnées suivantes :
```markdown
---
title: Changelog
---
```

## Licence
Ce projet est distribué sous la licence MIT. Voir le fichier [LICENSE](LICENSE.md) pour plus d'informations.

## Support
Pour obtenir de l'aide ou discuter du projet, vous pouvez ouvrir un problème dans ce dépôt ou contacter les mainteneurs directement.

