## Highlights

<!-- Text describing the most important changes in this release -->

### Fixes and improvements

<!-- List of fixes and improvements to the controllers and CLI -->

## New documentation

<!-- List of new documentation pages, if applicable -->

## Components changelog

- <name>-controller [v<version>](https://github.com/fluxcd/<name>-controller/blob/<version>/CHANGELOG.md

## CLI changelog

<!-- auto-generated list of pull requests to the CLI starts here -->