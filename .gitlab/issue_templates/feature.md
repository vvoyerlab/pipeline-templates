# Feature

## Description of Need

**As a** <role>  
**I want** <feature>  
**So that** <objective>

## Acceptance Criteria (Gherkin)

_**Tip**: Use Gherkin syntax to structure them. It's comprehensive and explicit, readable by humans but easy to convert into self-executable tests._

_**Advice**: A scenario title should describe in a complete sentence what happens in that scenario, to enhance readability and indentation._
**Good**: `The administrator logs into the admin interface`
**BAD**: `Logging into admin interface`

### Scenario title

**Given** <context>  
**When** <action>  
**Then** <result>

## Notes

*
*
*

> Source: [playbook.sparkfabrik.com - Gitlab issue templates](https://playbook.sparkfabrik.com/tools-and-policies/gitlab-issue-templates)