# Catalogue de Templates GitLab

## Description
Ce projet propose un catalogue de templates GitLab pour faciliter la configuration et le déploiement de pipelines CI/CD. Il vise à fournir une base solide pour automatiser les tests, le déploiement et d'autres étapes clés des workflows de développement logiciel.
