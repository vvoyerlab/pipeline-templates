---
title: Description
---

Execute build .nupkg and publish it.

## How to use it

```yml
- component: gitlab.com/vvoyerlab/pipeline-templates/nuget_package@1.0.0
    inputs:
      sdkVersion: '8.0'
```

## Inputs
| Name | Description | Default |
|--|--|--|
| job_name_suffix | Job name suffix |  |
| stage | Stage in which the job will run | integrate |
| sdkVersion | Dotnet sdk version to use |  |
| project_path | The project or solution to pack. It's either a path to a csproj, vbproj, or fsproj file, or to a solution file or directory. If not specified, the command searches the current directory for a project or solution file. |  |
| configuration | The configuration to use when running the tests. By default the job use Debug | Debug |
| package_version | Version of nuget package | $CI_COMMIT_TAG |
| output_directory | Places the built packages in the directory specified. | bin/Release |
| nuget_registry | Registry in which the package will be publish | ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/nuget/index.json |
| nuget_registry_name | Name of the registry | gitlab |
| nuget_registry_user | User to use to connect in to the registry | gitlab-ci-token |
| nuget_registry_password | User access token or password to use to connect in to the registry | $CI_JOB_TOKEN |
| environment | The environment name where to perform the integration | gitlab |
