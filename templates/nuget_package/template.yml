spec:
  inputs:
    job_name_suffix:
      description: 'Job name suffix'
      default: ''
    stage:
      description: 'Stage in which the job will run'
      default: integrate      
    sdkVersion:
      description: 'Dotnet sdk version to use'
    project_path:
      description: "The project or solution to pack. It's either a path to a csproj, vbproj, or fsproj file, or to a solution file or directory. If not specified, the command searches the current directory for a project or solution file."
      default: ''
    configuration:
      description: 'The configuration to use when running the tests. By default the job use Debug'
      default: Debug
      options:
        - Debug
        - Release
    package_version:
      description: 'Version of nuget package'
      default: '$CI_COMMIT_TAG'
    output_directory:
      description: 'Places the built packages in the directory specified.'
      default: 'artifacts'
    nuget_registry:
      description: 'Registry in which the package will be publish'
      default: '${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/nuget/index.json'
    nuget_registry_name:
      description: 'Name of the registry'
      default: 'gitlab'
    nuget_registry_user:
      description: 'User to use to connect in to the registry'
      default: 'gitlab-ci-token'
    nuget_registry_password:
      description: 'User access token or password to use to connect in to the registry'
      default: '$CI_JOB_TOKEN'
    environment:
      description: 'The environment name where to perform the integration'
      default: 'gitlab'
---

"nuget_package$[[ inputs.job_name_suffix ]]":
  stage: $[[ inputs.stage ]]
  image: mcr.microsoft.com/dotnet/sdk:$[[ inputs.sdkVersion ]]-alpine
  script:
    - dotnet pack $[[ inputs.project_path ]] -c $[[ inputs.configuration ]] --output $[[ inputs.output_directory ]]
    - dotnet nuget add source $[[ inputs.nuget_registry ]] --name $[[ inputs.nuget_registry_name ]] --username $[[ inputs.nuget_registry_user ]] --password $[[ inputs.nuget_registry_password ]] --store-password-in-clear-text
    - dotnet nuget push "$[[ inputs.output_directory ]]/*.nupkg" --source $[[ inputs.nuget_registry_name ]]
  after_script:
    - PACKAGE_NAME=$(basename "$[[ inputs.output_directory ]]/*.nupkg" .nupkg)
    - dotnet nuget list --source $[[ inputs.nuget_registry ]] $PACKAGE_NAME -AllVersions -Prerelease | grep -q "^${PACKAGE_NAME} $[[ inputs.package_version ]]$"
    - if [ $? -ne 0 ]; then
    -   echo "$PACKAGE_NAME:$[[ inputs.package_version ]] has been successfully published"
    - else
    -   echo "${PACKAGE_NAME}:$[[ inputs.package_version ]] not found"
    -   exit 1
    - fi
  environment: $[[ inputs.environment ]]