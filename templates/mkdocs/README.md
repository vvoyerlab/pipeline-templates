---
title: Description
---

Build mkdocs site and publish it.

## How to use it

```yml
- component: gitlab.com/vvoyerlab/pipeline-templates/mkdocs@1.0.0
    inputs:
      python_version: '3.8'
      documentation_folder: 'documentations'
```

## Inputs
| Name | Description | Default |
|--|--|--|
| stage | Stage in which the job will run | publish |
| python_version | Version of python to use |  |
| documentation_folder | Path to the documentation folder |  |
