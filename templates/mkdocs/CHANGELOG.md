---
title: Changelog
---

All notable changes to this job will be documented in this file.

## [1.0.0] - 2024-04-07
* Initial version
