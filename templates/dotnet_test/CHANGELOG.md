---
title: Changelog
---

All notable changes to this job will be documented in this file.

## [1.0.1] - 2024-02-24
* fix code coverage calculation
* use alpine sdk version

## [1.0.0] - 2024-02-24
* Initial version
