---
title: Description
---

Execute dotnet test in your project.

## How to use it

:warning: your test project must include [JunitXml.TestLogger package](https://www.nuget.org/packages/JunitXml.TestLogger).

```yml
- component: gitlab.com/vvoyerlab/pipeline-templates/dotnet_test@1.0.1
    inputs:
      sdkVersion: '8.0'
```

## Inputs

| Name          | Description                                     | Default |
|---------------|-------------------------------------------------|---------|
| stage         | stage in which the job will run                 | test    |
| sdkVersion    | dotnet sdk version to use                       |         |
| configuration | the configuration to use when running the tests | Debug   |
